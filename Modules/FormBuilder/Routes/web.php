<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth', 'form.exist'])->group(function () {
    Route::get('/', 'FormBuilderController@index');
    Route::prefix('form')->group(function() {
        Route::get('/field/{type}', 'FormBuilderController@loadOption');
        Route::post('/add', 'FormBuilderController@addFormElement');
        Route::post('/save', 'FormBuilderController@saveForm');
    });
});

Route::middleware(['auth', 'form.not.exist'])->prefix('my-form')->group(function () {
    Route::get('/', 'FormBuilderController@myForm');
    Route::post('/submit', 'FormBuilderController@submit');
});
