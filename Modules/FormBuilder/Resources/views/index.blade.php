@extends('formbuilder::layouts.master')

@section('content')



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Form Builder</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Starter Page</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-2">
                    <a class="btn btn-block bg-gradient-success" id="text" href="{{url('/form/field/text')}}">Add Text</a>
                </div>
                <div class="col-2">
                    <a type="button" class="btn btn-block bg-gradient-success" id="textarea" href="{{url('/form/field/textarea')}}">Add Textarea</a>
                </div>
                <div class="col-2">
                    <a type="button" class="btn btn-block bg-gradient-success" id="combo" href="{{url('/form/field/combo')}}">Add Combo</a>
                </div>
                <!-- <div class="col-2">
                    <a type="button" class="btn btn-block bg-gradient-success" id="datepicker" href="{{url('/form/field/date')}}">Add Date picker</a>
                </div> -->

                <div class="col-2">
                    <a type="button" class="btn btn-block bg-gradient-success" href="{{url('/form/field/check')}}">Add Checkox</a>
                </div>
                <div class="col-2">

                </div>
                <div class="col-2">

                </div>
            </div>

            <section id="form_preview">
                <div class="row">
                    <div class="col-sm-6 mt-5">
                        @if(isset($elements))
                        @foreach($elements as $element)
                        @if($element->type == "text")
                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{$element->label}}</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="{{$element->name}}">
                        </div>

                        @elseif($element->type == "textarea")
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">{{$element->label}}</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="{{$element->name}}"></textarea>
                        </div>
                        @elseif($element->type == "combo")
                        <div class="form-group">
                            <label for="exampleFormControlSelect2">{{$element->label}}</label>
                            <select class="form-control" id="exampleFormControlSelect2" name="{{$element->name}}">
                                @foreach(json_decode($element->option_label) as $key=>$label)
                                <option value="{{json_decode($element->option_value)[$key]}}">{{$label}}</option>
                                @endforeach
                            </select>
                        </div>

                        @elseif($element->type == "check")
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="{{$element->name}}" id="defaultCheck1">
                            <label class="form-check-label" for="defaultCheck1">
                                {{$element->label}}
                            </label>
                        </div>
                        @endif
                        @endforeach

                        @if($elements->count())
                        <div class="d-grid gap-2 mt-5">
                            <a type="button" id="save_form" class="btn btn-success">Save Form</a>
                        </div>
                        @endif
                        @endif
                    </div>
                </div>
            </section>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>
<!-- /.control-sidebar -->

<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
</footer>

<form action="{{url('/form/save')}}" method="post" id="save_form_submit">
    @csrf
</form>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#save_form').click(function() {
            if (confirm("Confirm to save your form") == true) {
                $('#save_form_submit').submit()
            }
        });
    });
</script>
@endsection