@extends('formbuilder::layouts.master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Form Builder</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Starter Page</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12 fb-text-options fb-options">
                    <div class="card mt-2">
                        <div class="card-body">
                            <h3 class="card-subtitle mb-2 text-muted">Options</h3>
                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <form action="{{url('/form/add')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="type" value="{{$type}}">
                                    <div class="row  p-2 mt-2 mb-3 element-name-con">
                                        <div class="col">
                                            <label>Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Name">
                                        </div>
                                        <div class="col">
                                            <label>Label</label>
                                            <input type="text" class="form-control" name="label" placeholder="Label">
                                        </div>
                                    </div>
                                    @if($type == 'combo')
                                    <label>Dropdown option</label>
                                    <div class="row border p-2">
                                        <div class="col-sm-12" id="select_options">
                                            <div class="row  p-2 mt-2 element-name-con">
                                                <div class="col">
                                                    <label>Label</label>
                                                    <input type="text" class="form-control form-control-sm" placeholder="Label" name="option_label[]">
                                                </div>
                                                <div class="col">
                                                    <label>Value</label>
                                                    <input type="text" class="form-control form-control-sm" placeholder="Value" name="option_value[]">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <button type="button" name="" id="add_option" class="btn btn-primary btn-sm">Add</button>
                                        </div>
                                    </div>
                                    @endif



                                    <label class="mt-2 d-block">validation</label>
                                    <div class="row">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input validations" type="checkbox" id="inlineCheckbox1" name="validation[]" value="required">
                                            <label class="form-check-label" for="inlineCheckbox1">Required</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input validations" type="checkbox" id="inlineCheckbox2" name="validation[]" value="numeric">
                                            <label class="form-check-label" for="inlineCheckbox2">Numeric</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input validations" type="checkbox" id="inlineCheckbox3" name="validation[]" value="string">
                                            <label class="form-check-label" for="inlineCheckbox3">String</label>
                                        </div>
                                    </div>
                                    <div class="d-grid gap-2 mt-4">
                                        <button type="submit" name="" id="" class="btn btn-success">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#add_option').click(function() {
            $('#select_options').append(
                '<div class="row  p-2 mt-2 element-name-con">\
                        <div class="col">\
                            <label>Label</label>\
                            <input type="text" class="form-control form-control-sm"  placeholder="field name" name="option_label[]">\
                        </div>\
                        <div class="col">\
                            <label>Value</label>\
                            <input type="text" class="form-control form-control-sm" placeholder="field label" name="option_value[]">\
                        </div>\
                    </div>'
            )
        });
    });
</script>
@endsection