<?php

namespace Modules\FormBuilder\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\FormBuilder\Entities\Form;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\FormBuilder\Entities\FormData;

class FormBuilderController extends Controller
{
    public function index()
    {
        $user_id = Auth::id();
        $elements = Form::where('user_id', $user_id)->get();
        return view('formbuilder::index', ['elements' => $elements]);
    }

    public function loadOption($type)
    {
        return view('formbuilder::options', ['type' => $type]);
    }
    public function addFormElement(Request $request)
    {
        $user_id = Auth::id();
        $request->validate([
            'name' => [
                'required',
                Rule::unique('forms')->where(fn ($query) => $query->where('user_id', $user_id)),
            ],
            'label' => 'required',
            'option_label.*' => Rule::requiredIf($request->type == "combo"),
            'option_value.*' => Rule::requiredIf($request->type == "combo"),
        ]);

        $form = new Form;
        $form->user_id = $user_id;
        $form->type = $request->type;
        $form->label = $request->label;
        $form->name = $request->name;
        $form->option_label = json_encode($request->option_label);
        $form->option_value = json_encode($request->option_value);
        $form->validation = json_encode($request->validation);
        $form->save();

        $elements = Form::where('user_id', $user_id)->get();
        return redirect('/');
    }

    public function myForm()
    {
        $user_id = Auth::id();
        $elements = Form::where('user_id', $user_id)->get();
        return view('formbuilder::my-form', ['elements' => $elements]);
    }

    public function submit(Request $request)
    {
        $user_id = Auth::id();
        $elements = Form::where('user_id', $user_id)->get();
        foreach ($elements as $element) {
            $name = $element->name;
            if (!is_null($element->validation)) {
                foreach (json_decode($element->validation) as $validation) {
                    $validator = Validator::make($request->all(), [
                        $name => $validation,
                    ]);
                    if ($validator->fails()) {
                        break;
                    }
                }
            }
            if ($validator->fails()) {
                break;
            }
        }
        if ($validator->fails()) {
            return $validator->errors()->first();
        }

        $data = json_encode($request->toArray());

        $FormData = new FormData;
        $FormData->user_id = Auth::id();
        $FormData->data = $data;
        $FormData->save();

        return "success";
    }

    public function saveForm()
    {
        $user_id = Auth::id();
        $user = User::find($user_id);
        $user->is_form_complete = 1;
        $user->save();

        return redirect('/my-form');
    }
}
